// method 1
var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function() {
        var text = reader.result;
        // var node = document.getElementById('output');
        // node.innerText = text;
        return text;
    };
    reader.readAsText(input.files[0]);
};

// method 2
// var file = "KnowledgeBase\\fhir-location-type.txt";

function readTextFile(file, index) {
    // alert("enter");
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4) {
            // alert("4");
            // alert("status :" + rawFile.status);
            if (rawFile.status === 200 || rawFile.status == 0) {
                // alert("enter");
                // alert("status :" + rawFile.status);
                var allText = rawFile.responseText;
                // alert(allText);
                switch (index) {
                    case 0:
                        appendTypeCont(allText);
                        break;
                    default:
                        break;
                }

                // alert("help");
            }
        }
    };
    rawFile.send(null);
}